# Payment Request for Browsers
New feature to easy fulfill data to make payments. Not all browsers support this feature, to get [more info](https://caniuse.com/#feat=payment-request)
[OficialDoc](https://www.w3.org/TR/payment-request/)

[Example](https://github.com/jorgecasar/payment-request)

* Objective: try new browser's feature **PaymentRequest**.
