/* 
The PaymentRequest constructor needs 3 argumetns
	- methodData: Sequence of PaymentMethodData that represent the payment methods that the site support
	- details: Details of the transaction (total cost, shipping options, goods...)
	- options: List of things that the site needs to deliver the good or service.
*/


const methodData= [
	{
		supportedMethods: "basic-card",
		data: {
			supportedNetworks: ["visa", "mastercard"],
			supportedTypes: ["debit", "credit"]
		}
	}
]

const details = {
  id: "super-store-order-123-12312",
  displayItems: [
    {
      label: "Sub-total",
      amount: { currency: "EUR", value: "55.00" },
    },
    {
      label: "Sales Tax",
      amount: { currency: "EUR", value: "5.00" },
    },
  ],
  total: {
    label: "Total due",
    // The total is 65.00€ here because we need to
    // add shipping (below). The selected shipping
    // costs 5.00€.  
    amount: { currency: "EUR", value: "65.00" },
  }
};

const shippingOptions = [
  {
    id: "standard",
    label: "🚛 Ground Shipping (2 days)",
    amount: { currency: "EUR", value: "5.00" },
    selected: true,
  },
  {
    id: "drone",
    label: "🚀 Drone Express (2 hours)",
    amount: { currency: "EUR", value: "25.00" }
  }
];

Object.assign(details, { shippingOptions });

const options = {
  requestPayerEmail: false,
  requestPayerName: true,
  requestPayerPhone: false,
  requestShipping: true,
}

/*
	Once we have all 3 arguments fulfillment, we can now try to build the PaymentRequest
*/

const request = new PaymentRequest(methodData, details, options);

request.onshippingaddresschange = ev => {
  ev.updateWith(checkShipping(request));
};
// Sync update to the total
request.onshippingoptionchange = ev => {
	console.log("shipingOptinoChanged: ", ev);
  const shippingOption = shippingOptions.find(
    option => option.id === ev.target.shippingOption
  );
  const newTotal = {
    currency: "EUR",
    label: "Total due",
    value: calculateNewTotal(shippingOption),
  };
  ev.updateWith({ ...details, total: {label: "new Total", amount: { currency: "EUR", value: "25.00" }}});
};

function calculateNewTotal(shipping) {
	console.log("shipping: ", shipping);
	return {
    label: "Total due",
    amount: { currency: "EUR", value: "85.00"} 
	};
}

async function checkShipping(request) {
  try {
    const json = request.shippingAddress.toJSON();

    await ensureCanShipTo(json);
    const { shippingOptions, total } = await calculateShipping(json);

    return { ...details, shippingOptions, total };
  } catch (err) {
    return { ...details, error: `Sorry! we can't ship to your address.` };
  }
}

async function doPaymentRequest() {
  try {
    //const request = new PaymentRequest(methodData, details, options);
    //request.onshippingaddresschange = ev => {console.log("event", ev); ev.updateWith(details);};
    //request.onshippingoptionchange = ev => {console.log("event", ev); ev.updateWith(details);};
    const response = await request.show();
    await validateResponse(response);
  } catch (err) {
    // AbortError, SecurityError
    console.error(err);
  }
}

async function validateResponse(response) {
  try {
console.log("Response: ", response);
		// By default we will vaildate all
		await response.complete("success");
    /*if (await checkAllValuesAreGood(response)) {
      await response.complete("success");
    } else {
      await response.complete("fail");
    }*/
  } catch (err) {
    // Something went wrong...
    await response.complete("fail");
  }
}

function pay() {
console.log("asking for paymentRequest");
	this.doPaymentRequest();
}
//doPaymentRequest();

